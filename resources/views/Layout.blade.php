<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  <title>FIBER ACADEMY</title>

  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <!-- DEMO ONLY: Function for the proper stylesheet loading according to the demo settings -->
  <script>function _pxDemo_loadStylesheet(a,b,c){var c=c||decodeURIComponent((new RegExp(";\\s*"+encodeURIComponent("px-demo-theme")+"\\s*=\\s*([^;]+)\\s*;","g").exec(";"+document.cookie+";")||[])[1]||"clean"),d="rtl"===document.getElementsByTagName("html")[0].getAttribute("dir");document.write(a.replace(/^(.*?)((?:\.min)?\.css)$/,'<link href="$1'+(c.indexOf("dark")!==-1&&a.indexOf("/css/")!==-1&&a.indexOf("/themes/")===-1?"-dark":"")+(!d||0!==a.indexOf("assets/css")&&0!==a.indexOf("assets/demo")?"":".rtl")+'$2" rel="stylesheet" type="text/css"'+(b?'class="'+b+'"':"")+">"))}</script>

  <!-- DEMO ONLY: Set RTL direction -->
  <script>"ltr"!==document.getElementsByTagName("html")[0].getAttribute("dir")&&"1"===decodeURIComponent((new RegExp(";\\s*"+encodeURIComponent("px-demo-rtl")+"\\s*=\\s*([^;]+)\\s*;","g").exec(";"+document.cookie+";")||[])[1]||"0")&&document.getElementsByTagName("html")[0].setAttribute("dir","rtl");</script>

  <!-- DEMO ONLY: Load PixelAdmin core stylesheets -->
  <script>
    _pxDemo_loadStylesheet('assets/css/bootstrap.min.css', 'px-demo-stylesheet-bs');
    _pxDemo_loadStylesheet('assets/css/pixeladmin.min.css', 'px-demo-stylesheet-core');
    _pxDemo_loadStylesheet('assets/css/widgets.min.css', 'px-demo-stylesheet-widgets');
  </script>

  <!-- DEMO ONLY: Load theme -->
  <script>
    function _pxDemo_loadTheme(a){var b=decodeURIComponent((new RegExp(";\\s*"+encodeURIComponent("px-demo-theme")+"\\s*=\\s*([^;]+)\\s*;","g").exec(";"+document.cookie+";")||[])[1]||"clean");_pxDemo_loadStylesheet(a+b+".min.css","px-demo-stylesheet-theme",b)}
    _pxDemo_loadTheme('assets/css/themes/');
  </script>

  <!-- Demo assets -->
  <script>_pxDemo_loadStylesheet('assets/demo/demo.css');</script>
  <!-- / Demo assets -->

  <!-- holder.js -->
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/holder/2.9.0/holder.js"></script>

  <!-- Pace.js -->
  <script src="assets/pace/pace.min.js"></script>

  <!-- Require.js -->
  <script src="assets/js/require.js"></script>
  <script src="assets/js/requirejs-config.js"></script>
  <script>
    requirejs.config({
      baseUrl: 'assets/js',

      // Demo script path
      paths: { demo: '../demo/demo' }
    });
  </script>

  <!-- Require demo script -->
  <script>require(['demo']);</script>

  <!-- Custom styling -->
  <style>
    .page-header-form .input-group-addon,
    .page-header-form .form-control {
      background: rgba(0,0,0,.05);
    }
  </style>
  <!-- / Custom styling -->
</head>
<body>
  <nav class="px-nav px-nav-left">
    <button type="button" class="px-nav-toggle" data-toggle="px-nav">
      <span class="px-nav-toggle-arrow"></span>
      <span class="navbar-toggle-icon"></span>
      <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
    </button>

    <ul class="px-nav-content">
      <li class="px-nav-box p-a-3 b-b-1" id="demo-px-nav-box">
        <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </li>

      <li class="px-nav-item px-nav-dropdown">
        <a href="#"><i class="px-nav-icon ion-ios-pulse-strong"></i><span class="px-nav-label">Fiber Academy<span class="label label-danger">1</span></span></a>

        <ul class="px-nav-dropdown-menu">
          <li class="px-nav-item"><a href="index.html"><span class="px-nav-label">DATA ABSENSI</span></a></li>
        </ul>
      </li>
    </ul>
  </nav>

  <nav class="navbar px-navbar">
    <!-- Header -->
    <div class="navbar-header">

      <a class="navbar-brand px-demo-brand" href="Home"><span class="px-demo-logo bg-primary"><span class="px-demo-logo-1"></span><span class="px-demo-logo-2"></span><span class="px-demo-logo-3"></span><span class="px-demo-logo-4"></span><span class="px-demo-logo-5"></span><span class="px-demo-logo-6"></span><span class="px-demo-logo-7"></span><span class="px-demo-logo-8"></span><span class="px-demo-logo-9"></span></span>INDIHOME</a>
    </div>


    <!-- Navbar togglers -->
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-demo-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

    <!-- Collect the nav links, forms, and other content for toggling -->
      <ul class="nav navbar-nav navbar-right">

        <li>
          <form class="navbar-form" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search" style="width: 140px;">
            </div>
          </form>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="assets/demo/avatars/1.jpg" alt="" class="px-navbar-image">
            <span class="hidden-md">User</span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="pages-profile-v2.html"><span class="label label-warning pull-xs-right"><i class="fa fa-asterisk"></i></span>Profile</a></li>
            <li><a href="pages-account.html">Account</a></li>
            <li class="divider"></li>
            <li><a href="pages-signin-v1.html"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
          </ul>
        </li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </nav>

  <div class="px-content">
    <ol class="breadcrumb page-breadcrumb">
      <li><a href="Home">Home</a></li>
      <li class="active">Fiber Academy</li>
    </ol>

    <div class="page-header">
      <div class="row">
        <div class="col-md-4 text-xs-center text-md-left text-nowrap">
          <h1><i class="page-header-icon ion-ios-pulse-strong"></i>Fiber Academy</h1>
        </div>

        <hr class="page-wide-block visible-xs visible-sm">

        <div class="col-xs-12 width-md-auto width-lg-auto width-xl-auto pull-md-right">
          <a href="#" class="btn btn-primary btn-block"><span class="btn-label-icon left ion-plus-round"></span>Create Data</a>
        </div>

        <!-- Spacer -->
        <div class="m-b-2 visible-xs visible-sm clearfix"></div>

        <form action="" class="page-header-form col-xs-12 col-md-4 pull-md-right">
          <div class="input-group">
            <span class="input-group-addon b-a-0 font-size-16"><i class="ion-search"></i></span>
            <input type="text" placeholder="Search..." class="form-control p-l-0 b-a-0">
          </div>
        </form>
      </div>
    </div>

  <div class="col-md-12" style="padding:10px;">
  
  @yield('content')
  
  </div>
  
  <ul class="px-nav-dropdown-menu">
              <li class="px-nav-item"><a href="pages-messages-list.html"><span class="px-nav-label">List</span></a></li>
              <li class="px-nav-item"><a href="pages-messages-item.html"><span class="px-nav-label">Item</span></a></li>
              <li class="px-nav-item"><a href="pages-messages-new.html"><span class="px-nav-label">New message</span></a></li>
            </ul>

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <script>
    // -------------------------------------------------------------------------
    // Initialize DEMO sidebar

    require(['jquery', 'demo', 'px/plugins/px-sidebar'], function($, pxDemo) {
      pxDemo.initializeDemoSidebar();

      $('#px-demo-sidebar').pxSidebar();
      pxDemo.initializeDemo();
    });
  </script>

  <script type="text/javascript">
    // -------------------------------------------------------------------------
    // Initialize DEMO

    require(['jquery', 'px/pixeladmin', 'px/plugins/px-nav', 'px/plugins/px-navbar', 'px/plugins/px-footer', 'px/extensions/perfect-scrollbar.jquery'], function($) {
      var file = String(document.location).split('/').pop();

      // Remove unnecessary file parts
      file = file.replace(/(\.html).*/i, '$1');

      if (!/.html$/i.test(file)) {
        file = 'index.html';
      }

      // Activate current nav item
      $('body > .px-nav')
        .find('.px-nav-item > a[href="' + file + '"]')
        .parent()
        .addClass('active');

      $('body > .px-nav').pxNav();
      $('body > .px-footer').pxFooter();

      $('#navbar-notifications').perfectScrollbar();
      $('#navbar-messages').perfectScrollbar();
    });
  </script>

  <script>
    // -------------------------------------------------------------------------
    // Initialize tabs

    require(['px-bootstrap/tab']);
  </script>

  <script>
    // -------------------------------------------------------------------------
    // Initialize uploads chart

    require(['demo', 'px-libs/morris'], function(pxDemo, Morris) {
      var data = [
        { day: '2014-03-10', v: pxDemo.getRandomData(20, 5) },
        { day: '2014-03-11', v: pxDemo.getRandomData(20, 5) },
        { day: '2014-03-12', v: pxDemo.getRandomData(20, 5) },
        { day: '2014-03-13', v: pxDemo.getRandomData(20, 5) },
        { day: '2014-03-14', v: pxDemo.getRandomData(20, 5) },
        { day: '2014-03-15', v: pxDemo.getRandomData(20, 5) },
        { day: '2014-03-16', v: pxDemo.getRandomData(20, 5) }
      ];

      new Morris.Line({
        element: 'hero-graph',
        data: data,
        xkey: 'day',
        ykeys: ['v'],
        labels: ['Value'],
        lineColors: ['#fff'],
        lineWidth: 2,
        pointSize: 4,
        gridLineColor: 'rgba(255,255,255,.5)',
        resize: true,
        gridTextColor: '#fff',
        xLabels: "day",
        xLabelFormat: function(d) {
          return ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov', 'Dec'][d.getMonth()] + ' ' + d.getDate();
        },
      });
    });

    // -------------------------------------------------------------------------
    // Initialize easy pie charts

    require(['jquery', 'demo', 'px-libs/jquery.easypiechart'], function($, pxDemo) {
      var colors = pxDemo.getRandomColors();

      var config = {
        animate: 2000,
        scaleColor: false,
        lineWidth: 4,
        lineCap: 'square',
        size: 90,
        trackColor: 'rgba(0, 0, 0, .09)',
        onStep: function(_from, _to, currentValue) {
          var value = $(this.el).attr('data-max-value') * currentValue / 100;

          $(this.el)
            .find('> span')
            .text(Math.round(value) + $(this.el).attr('data-suffix'));
        },
      }

      var data = [
        pxDemo.getRandomData(1000, 1),
        pxDemo.getRandomData(100, 1),
        pxDemo.getRandomData(64, 1),
      ];

      $('#easy-pie-chart-1')
        .attr('data-percent', (data[0] / 1000) * 100)
        .attr('data-max-value', 1000)
        .easyPieChart($.extend({}, config, { barColor: colors[0] }));

      $('#easy-pie-chart-2')
        .attr('data-percent', (data[1] / 100) * 100)
        .attr('data-max-value', 100)
        .easyPieChart($.extend({}, config, { barColor: colors[1] }));

      $('#easy-pie-chart-3')
        .attr('data-percent', (data[2] / 64) * 100)
        .attr('data-max-value', 64)
        .easyPieChart($.extend({}, config, { barColor: colors[2] }));
    });

    // -------------------------------------------------------------------------
    // Initialize retweets graph

    require(['jquery', 'demo', 'px/plugins/px-sparkline'], function($, pxDemo) {
      var data = [
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
      ];

      $("#sparkline-1").pxSparkline(data, {
        type: 'line',
        width: '100%',
        height: '42px',
        fillColor: '',
        lineColor: '#fff',
        lineWidth: 2,
        spotColor: '#ffffff',
        minSpotColor: '#ffffff',
        maxSpotColor: '#ffffff',
        highlightSpotColor: '#ffffff',
        highlightLineColor: '#ffffff',
        spotRadius: 4,
      });
    });

    // -------------------------------------------------------------------------
    // Initialize Monthly visitor statistics graph

    require(['jquery', 'demo', 'px/plugins/px-sparkline'], function($, pxDemo) {
      var data = [
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
        pxDemo.getRandomData(300, 100),
      ];

      $("#sparkline-2").pxSparkline(data, {
        type: 'bar',
        height: '42px',
        width: '100%',
        barSpacing: 2,
        zeroAxis: false,
        barColor: '#ffffff',
      });
    });

    // -------------------------------------------------------------------------
    // Initialize scrollbars

    require(['jquery', 'px/extensions/perfect-scrollbar.jquery'], function($) {
      $('#support-tickets').perfectScrollbar();
      $('#comments').perfectScrollbar();
      $('#threads').perfectScrollbar();
    });
  </script>
</body>
</html>
